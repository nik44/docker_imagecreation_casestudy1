*****Creating Your Own Image*****

Note:
1. This is a NodeJS app, You dont have to know anything about nodeJs to Dockerize it.
2. All the information need to write the Docker file for nodeJs is wrriten in the comment.

Procedure:
Step 1: Clone the NodeJS Repo from the URL: git clone https://nik44@bitbucket.org/nik44/docker_imagecreation_casestudy1.git

Step 2: GO to dockerfile in the repo 
    
           ->Create docker file using the appropriate command in the file
    
           -> All the path and instructions are provided in the comment line in the Dockerfile inside the repo

 Step 3:

 		-> Build Dockerfile

 	    -> Test it if it is running in the port

 	    -> Expected Test Result will be a Web Site at http://localhost

 Step 4:

 		-> Tag & push to your Free DockerHub account

 		-> Once it is done Remove your image in local cache in docker cli, Run it again from DockerHub
 		


